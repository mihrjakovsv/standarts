# Стандарты Javascript

## Стайл гайды

Для быстрой развертки проекта используем уже готовую сборку [Create React App](https://create-react-app.dev/), в которой уже настроен webpack среда для тестирования, много других полезных библиотек для работы со стилями, изображениями, и пр.

### Настройка ESLint и Pretties для create-react-app

- Устанавливаем [Prettier](https://prettier.io/)
  `npm install prettier --save-dev`

- Запускаем lint-stage, он настроить запуск Prettier на на хук pre-commit
  `npx mrm lint-staged`

После того, как команда отработает, установятся пакеты `husky` и `lint-staged`, а в package.json добавятся:

```json
{
  "husky": {
    "hooks": {
      "pre-commit": "lint-staged"
    }
  },
  "lint-staged": {
    "*.{js,css,md}": "prettier --write"
  }
}
```

- Устанавливаем плагины для связки ESLing и Prettier
  `npm install eslint-config-prettier eslint-plugin-prettier --save-dev`

- Добавляем настройки для ESLint в package.json

```json
{
  "eslintConfig": {
    "extends": ["react-app", "prettier"],
    "rules": {
      "jsx-quotes": [1, "prefer-double"],
      "no-console": 2,
      "no-debugger": 2
    },
    "plugins": ["prettier"]
  }
}
```

- Добавляем настройки для Prettier в package.json

```json
{
  "prettier": {
    "trailingComma": "es5",
    "tabWidth": 4,
    "semi": false,
    "singleQuote": true
  }
}
```

- Если в качестве редактора используется PhpStorm, то включаем ESLint в настройках проекта.
  Для этого переходим в `Settings/Languages & Frameworks/JavaScript/Code Quality Tools/ESLint` и включаем значение `Automatic ESLint configuration`

### Порядок методов в компоненте

[AirBnB](https://github.com/airbnb/javascript/tree/master/react#ordering)

## Ахитектура

### Структура папок проекта

Исходный код преимущественно находиться в папке `src`. Содержимое этой папки следующее:

```text
assets
    icons
        arrow.svg
        mark.svg
    images
        background.png
        logo.png
components
    App
    complexes
        Header
        Layout
    pages
        Home
        Catalog
        Admin
            Dashboard
            Goods
    simples
        Button
        Input
        Form
        Modal
    index.js
store
    ducks
    middlewares
    index.js
styles
    app.global.scss
    config.scss
utils
```

В папке `assets` все исползуемые в дизайне изображения и иконки.
В `components` находятся все React-компоненты. Они поделены на три группы по их сложности и области применения.
`simples` это самые простые элементарные компоненты: кнопки, элементы формы, и т.д. Все компоненты этой категории должны находиться на первом уровне вложенности.
`complexes` более сложные компоненты: Шапка, Меню, Футер, какие-то сложные части интерфейса. Могут содержать в себе более простые компоненты. Допускается использовать второй и в некоторых случаях третий уровень вложенности, если есть уверенность, что вложенные компоненты не будут использоваться ни где, кроме родительского компонента.
`pages` - целые страницы. Страницы преимущественно должны находиться на первом уровне вложенности, но могут быть поделены на большие группы, например, внешняя часть и административная часть.

Папка `store` является храниличем на базе React

Файл `styles/app.global.scss` определяет глобальные стили классы. Подключается в главном файле проекта. Здесь определяются стили сброса, глобальные настройки шрифта, глобальные классы (например, класс container);
`styles/config.scss` определяет глобальные переменные, функции, миксины. В последствии этот файл может быть подключен в стилях компоненов.

`utils` является хранилищем служебных функций.

### Структура компонента

```text
ComponentName
    ComponentName.jsx
    ComponentName.module.scss
    ComponentName.text.js
    index.js
```

Компонент должен быть отдельной папкой. Названия компонентам даются в [CamelCase](https://ru.wikipedia.org/wiki/CamelCase) нотации.
Для удобства работы в редакторе, чтобы во всех вкладках не отображалось `index.js`, основное содержимое комонента выносится в файл `ComponentName.jsx`, а `index.js` использоуется только для реимпорта.

```javascript
// index.js

export { default as ComponentName } from "./ComponentName";
```

### Рекомендации по работе с Redux

[Стайл гайд](https://redux.js.org/style-guide/style-guide)
[Immer](https://immerjs.github.io/immer/docs/faq)
[Нормализация состояния](https://redux.js.org/recipes/structuring-reducers/normalizing-state-shape)

### Настройка машрутизации

Инструкция по установке и подключению [React Router](https://reacttraining.com/react-router/web/guides/quick-start)
Использование React Router с Redux: https://redux.js.org/advanced/usage-with-react-router

### Работа с API, авторизация

[Normalizr](https://github.com/paularmstrong/normalizr)

### Использование Flow

### Документирование кода

## Тестирование

[Основы тестирования](https://ru.reactjs.org/docs/testing.html)
[Рецепты тестирования](https://ru.reactjs.org/docs/testing-recipes.html)
[Среды тестирования](https://ru.reactjs.org/docs/testing-environments.html)
[Запуск тестов в Create React App](https://create-react-app.dev/docs/running-tests)
[Дебагинг тестов в Create React App](https://create-react-app.dev/docs/debugging-tests)
[Написание тестов для React](https://redux.js.org/recipes/writing-tests)
[Современное тестирование React](https://blog.sapegin.me/all/react-testing-1-best-practices/)
https://medium.com/selleo/testing-react-components-best-practices-2f77ac302d12
https://medium.com/@noahpeden/react-testing-library-rtl-set-up-and-best-practices-96dcf3be54
https://techblog.commercetools.com/testing-in-react-best-practices-tips-and-tricks-577bb98845cd

## Подготовка для деплоя

### Настройка докерфайл

Содержимое Dockerfile

```
# Stage 1 - the build process
FROM node:10 as build-deps
WORKDIR /usr/src/app
COPY package*.json ./
RUN npm install
COPY . ./
RUN npm run build

# Stage 2 - the production environment
FROM nginx:1.12-alpine
COPY --from=build-deps /usr/src/app/build /usr/share/nginx/html
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
```

Создаем файл .dockerignore и исключаем папку `node_modules`.

```
node_modules
```

## Советы по редакторам

### PHPStorm

Существуют следующие полезные плагины:

- React snippets
- ReactPropTypes

Часто используемые сниппеты:

- `cdc` - создает componentDidCatch
- `cdm` - создает componentDidMount
- `cdup` - создает componentDidUpdate
- `con` - создает constructor(prop) {}
- `cwun` - создает componentWillUnmount() {}
- `disp` - mapDispatchToProps - перенастроить (надо перенастроить)
- `hoc` - Создает HOC
- `rcc` - создает пустой классовый комонент, наследуемый от React.Component
- `rccp` - создает классовый компонент с propTypes, наследуемы от React.Component
- `rpc` - создает классовый компонент с propTypes, наследуемый от React.PureComponent
- `rrc` - создает классовый компонент с подключением к Redux
- `rsc` - создает функциональный компонент при помощи стрелочной функции
- `rscp` - создает функциональный компонент при помощи стрелочной функции с propTypes
- `rsf` - создает функциональный компонент
- `rsfp` - создает функциональный компонент с propTypes
- `useCallback`
- `useEffect`
- `useState`

## Используемые материалы

### Статьи с примерами струкруты папок

https://medium.com/@Charles_Stover/optimal-file-structure-for-react-applications-f3e35ad0a145
https://medium.com/@Charles_Stover/writing-testable-react-components-with-hooks-23441ee582d5
https://www.robinwieruch.de/react-folder-structure
https://www.pluralsight.com/guides/file-structure-react-applications-created-create-react-app
https://daveceddia.com/react-project-structure/
https://blog.bitsrc.io/structuring-a-react-project-a-definitive-guide-ac9a754df5eb

### Использование SCSS модулей

https://github.com/css-modules/css-modules
https://medium.com/clover-platform-blog/modular-scss-and-why-you-need-it-6bb2d8c40fd8
[Reset CSS](https://gist.github.com/DavidWells/18e73022e723037a50d6)
